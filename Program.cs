﻿using ChartDrawer;
using System.Collections.Generic;

namespace LAB4
{
	class Program
	{
		static void Main(string[] args)
		{
			var points = new List<Point>();
			for (int i = 1; i <= 40; i++)
			{
				var point = new Point(i, ++i);
				points.Add(point);
			}

			AdditionalInfoCharts chart = new AdditionalInfoCharts();
			chart.GenerateChart("Tytuł wykresu", "X", "Y", points, "aaa.jpg");
		}
	}
}
