﻿using ChartDrawer;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace LAB4
{
	public class AdditionalInfoCharts : Charts
	{ 
		public override void GenerateChart(string title, string xlabel, string ylabel, List<Point> points, string filename)
		{
			base.GenerateChart(title, xlabel, ylabel, points, filename);

			var text = PrepareTxtFile(title, points);

			File.WriteAllText("dodatkowe_informacje.txt", text);
			OpenFileAfterCreate("dodatkowe_informacje.txt");
		}

		private string PrepareTxtFile(string title, List<Point> points)
		{
			StringBuilder pointString = new StringBuilder();
			pointString.AppendLine(title);
			foreach (var point in points)
			{
				pointString.AppendLine("X = " + point.X + " Y = " + point.Y);
			}
			return pointString.ToString();
		}

		private void OpenFileAfterCreate(string name)
		{
			var p = new Process();
			p.StartInfo = new ProcessStartInfo(name)
			{
				UseShellExecute = true
			};
			p.Start();
		}
	}
}
